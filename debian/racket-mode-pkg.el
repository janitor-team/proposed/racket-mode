;;; -*- no-byte-compile: t -*-
(define-package "racket-mode" "20230425snapshot0" "Major mode for Racket language."
  '((pos-tip "0.4.6") (s "1.9.0")) :url "https://github.com/greghendershott/racket-mode")
